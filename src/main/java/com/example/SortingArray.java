package com.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SortingArray
{
    private static final Logger logger = LogManager.getLogger(SortingArray.class);

    public static String sort(String[] args )
    {

        logger.info("Sorting started.");

        if (args.length == 0) {
            logger.error("No command-line arguments provided.");
            throw new IllegalArgumentException();
        }

        if (args.length > 10) {
            logger.error("Too many command-line arguments provided. Maximum allowed is 10.");
            throw new IllegalArgumentException();
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                logger.error("Invalid input: \"" + args[i] + "\" is not an integer.");
                throw new IllegalArgumentException();
            }
        }

        bubbleSort(numbers); // Replace Arrays.sort(numbers) with your custom sorting algorithm

        logger.info("Sorting completed.");


        StringBuilder result = new StringBuilder("Sorted numbers:");
        for (int num : numbers){
            result.append(" ").append(num);
        }

        System.out.println(result);
        return result.toString();
    }

    private static void bubbleSort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
}
