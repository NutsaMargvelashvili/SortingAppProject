package com.example;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;



@RunWith(Parameterized.class)
public class SortingArrayParametrizedTest {

    private String[] inputArgs;
    private String expectedOutput;

    public SortingArrayParametrizedTest(String[] inputArgs, String expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"9", "5", "7", "2", "3"}, "Sorted numbers: 2 3 5 7 9"},
                {new String[]{"1", "2", "3", "4", "5"}, "Sorted numbers: 1 2 3 4 5"}
        });
    }

    @Test
    public void testSorting() {
        assertEquals(expectedOutput, SortingArray.sort(inputArgs));
    }
}
