package com.example;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Unit test for simple App.
 */
public class SortingArrayTest extends TestCase
{

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyArguments() {
        String[] args = {};
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            SortingArray.sort(args);
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooManyArguments() {
        String[] args = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            SortingArray.sort(args);
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidArguments() {
        String[] args = {"1", "abc", "3"};
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            SortingArray.sort(args);
        });
    }

    @Test
    public void testOneArgument() {
        String[] args = {"5"};
        String expectedOutput = "Sorted numbers: 5";
        assertEquals(expectedOutput, SortingArray.sort(args));
    }

    @Test
    public void testTenArguments() {
        String[] args = {"9", "5", "7", "2", "3", "8", "1", "6", "4", "0"};
        String expectedOutput = "Sorted numbers: 0 1 2 3 4 5 6 7 8 9";
        assertEquals(expectedOutput, SortingArray.sort(args));
    }

}
